package handlers

import (
	"context"
	"encoding/json"
	"net/http"
	"time"
	db "user/Database"
	model "user/Model"

	"github.com/gorilla/mux"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

// Create User Handler
func CreateUser(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	var user model.User
	json.NewDecoder(request.Body).Decode(&user)
	if !user.ValidateInput(response) {
		return
	}
	user.Created = time.Now()
	user.Updated = time.Now()
	user.IsActive = true
	collection := db.ConnectDB()
	result, err := collection.InsertOne(context.TODO(), user)
	if err != nil {
		http.Error(response, "User could not be created", http.StatusBadRequest)
		return
	}
	user.ID = result.InsertedID.(primitive.ObjectID)
	json.NewEncoder(response).Encode(user)
}

// Get All users Handler
func GetAllUsers(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	collection := db.ConnectDB()
	cursor, err := collection.Find(context.TODO(), bson.M{})
	if err != nil {
		http.Error(response, "User details could not be fetched", http.StatusBadRequest)
		return
	}
	defer cursor.Close(context.TODO())
	var users []model.User
	for cursor.Next(context.TODO()) {
		var user model.User
		err := cursor.Decode(&user)
		if err != nil {
			//Error
			http.Error(response, "Error decoding user details", http.StatusBadRequest)
			return
		}
		if user.IsActive {
			users = append(users, user)
		}
	}
	json.NewEncoder(response).Encode(users)

}

// Get user by ID Handler
func GetUserById(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	var RouteVariables = mux.Vars(request)
	var user model.User
	id, er := primitive.ObjectIDFromHex(RouteVariables["id"])
	if er != nil {
		http.Error(response, "Route variable is not a valid ObjectID", http.StatusBadRequest)
		return
	}
	filter := bson.M{"_id": id}
	collection := db.ConnectDB()
	err := collection.FindOne(context.TODO(), filter).Decode(&user)
	if err != nil {
		http.Error(response, "User detail could not be fetched", http.StatusBadRequest)
		return
	}
	if !user.IsActive {
		http.Error(response, "User does not exist", http.StatusNotFound)
		return
	}
	json.NewEncoder(response).Encode(user)

}

// Update User Handler
func UpdateUser(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	var RouteVariables = mux.Vars(request)
	var user model.User
	id, er := primitive.ObjectIDFromHex(RouteVariables["id"])
	if er != nil {
		http.Error(response, "Route variable is not a valid ObjectID", http.StatusBadRequest)
		return
	}
	json.NewDecoder(request.Body).Decode(&user)
	if !user.ValidateInput(response) {
		return
	}
	filter := bson.M{"_id": id}
	updatePayload := bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "updated", Value: time.Now()},
			{Key: "firstName", Value: user.FirstName},
			{Key: "lastName", Value: user.LastName},
			{Key: "age", Value: bson.D{
				{Key: "value", Value: user.Age.Value},
				{Key: "interval", Value: user.Age.Interval},
			}},
			{Key: "mobile", Value: user.Mobile},
		}},
	}
	collection := db.ConnectDB()
	err := collection.FindOneAndUpdate(context.TODO(), filter, updatePayload).Decode(&user)
	if err != nil {
		http.Error(response, "User details could not be updated", http.StatusBadRequest)
		return
	}
	json.NewEncoder(response).Encode(user)
}

// Delete User Handler

func DeleteUser(response http.ResponseWriter, request *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	var RouteVariables = mux.Vars(request)
	var user model.User
	id, er := primitive.ObjectIDFromHex(RouteVariables["id"])
	if er != nil {
		http.Error(response, "Route variable is not a valid ObjectID", http.StatusBadRequest)
		return
	}
	json.NewDecoder(request.Body).Decode(&user)
	filter := bson.M{"_id": id}
	update := bson.D{
		{Key: "$set", Value: bson.D{
			{Key: "isActive", Value: false},
		}},
	}

	collection := db.ConnectDB()
	err := collection.FindOneAndUpdate(context.TODO(), filter, update).Decode(&user)
	if err != nil {
		http.Error(response, "Error deleting user details", http.StatusBadRequest)
		return
	}
	json.NewEncoder(response).Encode(user)

}
