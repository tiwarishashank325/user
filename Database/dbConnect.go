package database

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func ConnectDB() *mongo.Collection {

	clientOptions := options.Client().ApplyURI("mongodb://mongo1:27017")
	client, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		log.Fatal("Connection Error", err)
	}
	fmt.Println("Database Connected")
	collection := client.Database("ShashankDB").Collection("UserDetails")
	return collection
}
