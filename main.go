package main

import (
	"log"
	"net/http"
	handlers "user/Handlers"

	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/v1/users", handlers.CreateUser).Methods("POST")
	router.HandleFunc("/v1/users", handlers.GetAllUsers).Methods("GET")
	router.HandleFunc("/v1/users/{id}", handlers.GetUserById).Methods("GET")
	router.HandleFunc("/v1/users/{id}", handlers.UpdateUser).Methods("PUT")
	router.HandleFunc("/v1/users/{id}", handlers.DeleteUser).Methods("DELETE")
	log.Fatal(http.ListenAndServe(":8000", router))
}
