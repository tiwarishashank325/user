package model

import (
	"net/http"
	"regexp"
	"time"

	"github.com/go-playground/validator"
	"go.mongodb.org/mongo-driver/bson/primitive"
)

type Age struct {
	Value    int    `json:"value,omitempty" bson:"value,omitempty" validate:"numeric"`
	Interval string `json:"interval,omitempty" bson:"interval, omitempty" validate:"oneof=years months days weeks"`
}

type User struct {
	ID        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	Created   time.Time          `json:"created,omitempty" bson:"created,omitempty"`
	Updated   time.Time          `json:"updated,omitempty" bson:"updated,omitempty"`
	FirstName string             `json:"firstName,omitempty" bson:"firstName,omitempty" validate:"fname"`
	LastName  string             `json:"lastName,omitempty" bson:"lastName,omitempty" validate:"lname"`
	Age       Age                `json:"age,omitempty" bson:"age,omitempty" validate:"required"`
	Mobile    string             `json:"mobile,omitempty" bson:"mobile,omitempty" validate:"required,mob"`
	IsActive  bool               `json:"isActive" bson:"isActive"`
}

var r http.ResponseWriter
var ErrorMessage = false

func (u *User) ValidateInput(rw http.ResponseWriter) bool {
	v := validator.New()
	r = rw
	v.RegisterValidation("fname", validateFirstName)
	v.RegisterValidation("lname", validatelastName)
	v.RegisterValidation("mob", validateMobile)
	err := v.Struct(u)
	if err != nil {
		if !ErrorMessage {
			http.Error(rw, "Invalid input for Age ", http.StatusBadRequest)
		}
		return false
	}
	return true

}
func validateFirstName(fl validator.FieldLevel) bool {
	isAlpha := regexp.MustCompile(`^[A-Za-z]+$`).MatchString
	name := fl.Field().String()
	if !isAlpha(name) {
		ErrorMessage = true
		http.Error(r, "First Name should contain only alphabets", http.StatusBadRequest)
		return false
	}

	return true
}

func validateMobile(fl validator.FieldLevel) bool {
	isNumber := regexp.MustCompile(`^[0-9]+$`).MatchString
	number := fl.Field().String()
	if !isNumber(number) {
		ErrorMessage = true
		http.Error(r, "Enter Valid Mobile Number", http.StatusBadRequest)
		return false
	}

	return true
}
func validatelastName(fl validator.FieldLevel) bool {
	isAlpha := regexp.MustCompile(`^[A-Za-z]+$`).MatchString
	name := fl.Field().String()
	if !isAlpha(name) {
		ErrorMessage = true
		http.Error(r, "Last Name should contain only alphabets", http.StatusBadRequest)
		return false
	}

	return true
}
